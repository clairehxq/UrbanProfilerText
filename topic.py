def compareItems((w1,c1), (w2,c2)):
    if c1 > c2:
        return - 1
    elif c1 == c2:
        return cmp(w1, w2)
    else:
        return 1
    
def wordcount(cleanlist):
    counts = {}
    for w in cleanlist:
        if (w.lower() in stopw) or (w == ''):
            continue
        else:
            counts[w.lower()] = counts.get(w.lower(),0) + 1
    return counts

def freqword(cleanlist):
    '''
    return a list of word by their descending order of occurances
    '''
    countlist = wordcount(cleanlist)
    try:
        countlist_sort = sorted(countlist.keys(), key = countlist.__getitem__, reverse = True)
    except IndexError:
        countlist_sort = np.nan
    return countlist_sort

def clean(doc):
    from stop_words import get_stop_words
    from nltk.stem.wordnet import WordNetLemmatizer
    import string
    
    stopw = _stop_words('en')
    exclude = set(string.punctuation) 
    lemma = WordNetLemmatizer()

    doc = doc.replace('/', ' ')
    stop_free = " ".join([i for i in doc.lower().split() if i not in stopw])
    punc_free = ''.join(ch for ch in stop_free if ch not in exclude)
    normalized = " ".join(lemma.lemmatize(word) for word in punc_free.split())
    return normalized

def topic_dict(data):

    import gensim
    from gensim import corpora
    
    doc_complete = data.content
    doc_clean = [clean(doc).split() for doc in doc_complete]  

    # Creating the term dictionary of our courpus, where every unique term is assigned an index. 
    dictionary = corpora.Dictionary(doc_clean)
    return dictionary

def topic_doc_term_matrix(data):
    dictionary = topic_dict(data)
    doc_clean = [clean(doc).split() for doc in doc_complete]
    doc_term_matrix = [dictionary.doc2bow(doc) for doc in doc_clean]
    return doc_term_matrix

    
def similar_topic(t1, t2):
    '''
    returns true if two topics are similar
    can be more precise
    '''
    t1 = clean(t1).split()
    t2 = clean(t2).split()
    if (len(list(set(t1) & set(t2))) > 0) & (t1 != t2): #have overlapping word & isn't itself
        return True
    else:
        return False

def compare_topic(t1, topiclist):
    stopic = []
    for t2 in topiclist:
        stopic.append(similar_topic(t1,t2))
    return topiclist[stopic]

def count_word_freq_in_other_topic(dtmatrix, topiclist):
    dt_dic = {}
    for i,dt in enumerate(dtmatrix):
        topic = topiclist[i]
        stopic_id = compare_topic(topic, topiclist).index #similar topics
        dt_new = []
        for pair in dt:
            doc, num = pair
            word = dictionary.id2token.get(doc, 0)
            occ = all_freq[doc] - num #all occurences - occurance in this topic
            
            bool_overl_topic = similar_topic(word, topic)
            for st_id in stopic_id: #all occurences - occurance in similar topics
                st_w = dict(dtmatrix[st_id])
                s_num = st_w.get(doc, 0)
                occ -= s_num
                
            # new pairs return word_id, num_occur_this_topic, num_occur_non_related_topic, bool_overlap_topic
            if occ != 0:
                score = num/occ + bool_overl_topic
            else:
                score = bool_overl_topic
            pair_new = (doc, num, occ, bool_overl_topic)
            dt_new.append(pair_new)
        dt_dic[i] = dt_new
       
    return dt_dic