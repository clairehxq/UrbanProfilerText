# These are written for fun (actually abundent functions and I just can't delete them all)
def compareItems((w1,c1), (w2,c2)):
    if c1 > c2:
        return - 1
    elif c1 == c2:
        return cmp(w1, w2)
    else:
        return 1
    
def wordcount1(cleanlist):
    counts = {}
    for w in cleanlist:
        if (w.lower() in stopw) or (w == ''):
            continue
        else:
            counts[w.lower()] = counts.get(w.lower(),0) + 1
    return counts

def freqword(cleanlist):
    '''
    return a list of word by their descending order of occurances
    '''
    countlist = wordcount(cleanlist)
    try:
        countlist_sort = sorted(countlist.keys(), key = countlist.__getitem__, reverse = True)
    except IndexError:
        countlist_sort = np.nan
    return countlist_sort

# cleaning text
def clean(doc):
    from stop_words import get_stop_words
    from nltk.corpus import stopwords
    from nltk.stem.wordnet import WordNetLemmatizer
    import string
    
    stopw = get_stop_words('en')
    #stopw = stopwords.words('english')
    exclude = set(string.punctuation) 
    lemma = WordNetLemmatizer()

    doc = doc.replace('/', ' ')
    stop_free = " ".join([i for i in doc.lower().split() if i not in stopw])
    punc_free = ''.join(ch for ch in stop_free if ch not in exclude)
    normalized = " ".join(lemma.lemmatize(word) for word in punc_free.split())
    return normalized

def clean_topic(doc):
    from stop_words import get_stop_words
    from nltk.stem.wordnet import WordNetLemmatizer
    import string
    
    stopw = get_stop_words('en')
    exclude = set(string.punctuation) 
    lemma = WordNetLemmatizer()

    doc = doc.apply(lambda x: x.replace('/', ' ').replace('-',''))
    stop_free = [i.lower().split() for i in doc]
    punc_free = [ch for ch in stop_free if ch!=',']

    return punc_free

# on topics
def topic_dict(data):

    import gensim
    from gensim import corpora
    
    doc_complete = data.content
    doc_clean = [clean(doc).split() for doc in doc_complete]  

    # Creating the term dictionary of our courpus, where every unique term is assigned an index. 
    dictionary = corpora.Dictionary(doc_clean)
    return dictionary

def topic_doc_term_matrix(data):
    dictionary = topic_dict(data)
    doc_clean = [clean(doc).split() for doc in doc_complete]
    doc_term_matrix = [dictionary.doc2bow(doc) for doc in doc_clean]
    return doc_term_matrix

    
def similar_topic(t1, t2):
    '''
    returns true if two topics are similar
    can be more precise
    '''
    t1 = clean(t1).split()
    t2 = clean(t2).split()
    if (len(list(set(t1) & set(t2))) > 0) & (t1 != t2): #have overlapping word & isn't itself
        return True
    else:
        return False

def compare_topic(t1, topiclist):
    stopic = []
    for t2 in topiclist:
        stopic.append(similar_topic(t1,t2))
    return topiclist[stopic]

# grouping topics
def get_group(cleantopiclist):
    '''
    from topic list doc,
    find the 20 most frequent words
    and set them as 20 groups
    '''
    from gensim import corpora
    wdict_t = corpora.Dictionary(cleantopiclist)
    # dtm: doc_term_matrix
    dtm_t = [wdict_t.doc2bow(doc) for doc in cleantopiclist]
    group = {}
    flatten = lambda l: [item for sublist in l for item in sublist]
    for k,v in flatten(dtm_t):
        group[k] = group.get(k,0) + v

    return [wdict_t[i] for i in sorted(group, key =group.__getitem__, reverse = True)[:20]]

def group_topic(cleantopiclist):
    from collections import defaultdict
    groups = get_group(cleantopiclist)
    grouping = defaultdict(list)

    for topic in cleantopiclist:
        try:
            g = list(set(topic) & set(groups))[0]
        except IndexError:
            g = 0
        grouping[g].append(topic)
        
    return grouping

# topics
def count_word_freq_in_other_topic(dtmatrix, topiclist):
    dt_dic = {}
    for i,dt in enumerate(dtmatrix):
        topic = topiclist[i]
        stopic_id = compare_topic(topic, topiclist).index #similar topics
        dt_new = []
        for pair in dt:
            doc, num = pair
            word = dictionary.id2token.get(doc, 0)
            occ = all_freq[doc] - num #all occurences - occurance in this topic
            
            bool_overl_topic = similar_topic(word, topic)
            for st_id in stopic_id: #all occurences - occurance in similar topics
                st_w = dict(dtmatrix[st_id])
                s_num = st_w.get(doc, 0)
                occ -= s_num
                
            # new pairs return word_id, num_occur_this_topic, num_occur_non_related_topic, bool_overlap_topic
            if occ != 0:
                score = num/occ + bool_overl_topic
            else:
                score = bool_overl_topic
            pair_new = (doc, num, occ, bool_overl_topic)
            dt_new.append(pair_new)
        dt_dic[i] = dt_new
       
    return dt_dic

def wordcount(cleanlist, wdic):
    '''
    only counts words in topic corpus
    returns a (word_id : counts) dictionary'''
    counts = {}
    word_id = wdic
    
    for w in cleanlist:
        # only do counts for words in id2token
        if (w in word_id.values()):
            counts[w.lower()] = counts.get(w.lower(),0) + 1
        else:
            continue
    #convert word to id        
    for j in range(len(counts.keys())):
        search_word = counts.keys()[j]
        for wid, word in word_id.iteritems():
            
            if word == search_word:
                counts[wid] = counts.pop(search_word)
    return counts

# scoring & matching
def get_cosine(vec1, vec2):
    '''
    vec1 and vec 2 are both dictionary
    returns cosine similarity in float
    '''
    import math
    intersection = set(vec1.keys()) & set(vec2.keys())
    numerator = sum([vec1[x] * vec2[x] for x in intersection])

    sum1 = sum([vec1[x]**2 for x in vec1.keys()])
    sum2 = sum([vec2[x]**2 for x in vec2.keys()])
    denominator = math.sqrt(sum1) * math.sqrt(sum2)

    if not denominator:
        return 0.0
    else:
        return float(numerator) / denominator

def cosine_sim(des, wdic, doc_term_matrix):
    '''
    calculating the cosine similarity between
    new description and topic doc_term_matrix
    returns a list of cosine similarity
    '''

    sim = {}
    wcvec = wordcount(clean(des).split(), wdic)
    for i in range(len(doc_term_matrix)):
        sim[i] = get_cosine(wcvec, dict(doc_term_matrix[i]))
    
    # sort by cosine similarities
    sim = sorted(sim.items(), key = lambda x: x[1], reverse = True)
    return sim

def get_best_topic(new_des, topic_list, wdic, matrix):
    '''
    new_des as a string,
    topic_list is the true list,
    wdic is the id2token dictionary,
    matrix is the doc_term_matrix,
    '''
    pred_null = 0
    try:
        pred_topic = topic_list[cosine_sim(new_des, wdic, matrix)[0][0]]
    except (ValueError, AttributeError): #while some descriptor is null
        pred_topic = new_des  
        pred_null += 1
    return pred_topic, pred_null

# performance
def acc_per(test, topic_list, wdic, matrix):
    null_time = 0
    true_positive = 0
    for row in test.itertuples():
        pred, null_val = get_best_topic(row[5], topic_list, wdic, matrix)
        null_time += null_val
        if pred == row[4]:
            true_positive += 1
    
    return 1.0 * (true_positive - null_time)/(len(test)-null_time)