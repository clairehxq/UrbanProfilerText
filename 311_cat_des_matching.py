import similarity #refer to similarity.py
import pandas as pd
import numpy
from gensim import corpora
import math
import sys

# load data
use_column = ['Unique Key', 
              'Agency', 
              'Agency Name',
              'Complaint Type', 
              'Descriptor',
              'Location Type', 
              'Incident Address',
             ]
data_path = '/scratch/xh895/UrbanProfilerText/data/311_dl_32817.csv'
data = pd.read_csv(data_path, 
                   usecols = use_column,
                   nrows = 110000)

test_data = data[100000:-1]
data1 = data[:100000].groupby(['Complaint Type', 'Location Type', 'Descriptor'], 
                     as_index=False, 
                     group_keys = False)\
                    .count()\
                    .iloc[:, :4]\
                    .rename(columns = {'Unique Key': 'count'})

data1.Descriptor = data1.Descriptor.apply(lambda x: x + ' ')
data1['Location Type'] = data1['Location Type'].apply(lambda x: x + ' ')

# see how words in 'Descriptor' associates with each 'Complaint Type'
data2 = data1.iloc[:,:3].groupby('Complaint Type', as_index = False, group_keys = False).sum()
data2['content'] = data2['Location Type'] + data2['Descriptor']

# compile documents
doc_complete = data2.content

doc_complete = data2.content
doc_clean = [similarity.clean(doc).split() for doc in doc_complete]  
# wdict is the id2token dictionary
wdict = corpora.Dictionary(doc_clean)
# dtm: doc_term_matrix
dtm = [wdict.doc2bow(doc) for doc in doc_clean]
topic_true = data2['Complaint Type']

# test single row:
row = test_data.loc[100001]

print 'to test Descriptor:', row.Descriptor
print 'true topic:', row['Complaint Type']
print 'predict topic:', similarity.get_best_topic(row.Descriptor,
                                                 topic_true,
                                                 wdict,
                                                  dtm)[0]


print "if 'Location Type' is not added to descriptor, print accuracy:"
accuracy_noLT =  similarity.acc_per(test_data,
                        topic_true,
                        wdict,
                        dtm)
print accuracy_noLT

print "add 'Location Type' to test data:"

test_data['Descriptor'] = test_data['Descriptor'] + test_data['Location Type'].astype(str) 
accuracy_LT = similarity.acc_per(test_data,
                        topic_true,
                        wdict,
                        dtm)
print accuracy_LT

# group Complaint Types - topic refers to Complaint Type
topicl = similarity.clean_topic(topic_true)
grouped = similarity.group_topic(topicl)
print 'Noise Group contains topics:', grouped['noise']

print 'Note: Need more sophisticated grouping functions: how to determine which group a topic belongs to if it interacts with multiple group keywords'

print 'group and corresponding topics: (group 0 means "isolated" topic)'
print grouped.items()