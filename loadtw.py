import pandas as pd
import numpy
import nltk
from nltk.corpus import stopwords 
from nltk.stem.wordnet import WordNetLemmatizer
import string
import gensim
from gensim import corpora
import seaborn as sns
from stop_words import get_stop_words
import re
import matplotlib.pyplot as plt
from nltk.corpus import stopwords 
from nltk.stem.wordnet import WordNetLemmatizer
import gensim
from gensim import corpora
import string
import pyspark
import operator 
%pylab inline

data_path = '/scratch/share/xh895/tw/twitter_drug_2017_01_01.jsonl'

sc = pyspark.SparkContext()
sp = sc.textFile(data_path)

def mp_content(rows):
    for row in rows:
        allcont =  row.split(',')
        for cont in allcont:
            if cont.startswith('"text"'):
                yield cont.split(':')[1][1:-1]


tw_23 = sp.mapPartitions(mp_content)

drug_names = pd.read_excel('Illegal Drug Names.xlsx')

def druglist(cell):
    try:
        return cell.lower().split(', ')
    except AttributeError:
        return ['']
    
drug = reduce(operator.add, drug_names.applymap(druglist).iloc[:,0])
sn_sin = reduce(operator.add, drug_names.applymap(druglist).iloc[:,1])
sn_dou = reduce(operator.add, drug_names.applymap(druglist).iloc[:,2])
sn_non = reduce(operator.add, drug_names.applymap(druglist).iloc[:,3])

# all_drugs contains precription names & street names
all_drugs = drug + sn_sin + sn_dou + sn_non
all_drugs = [x for x in all_drugs if x != '']

drug_name_sim = pd.read_csv('Rx Drugs List (1).csv')
drug_name_com = pd.read_csv('Rx Drugs List.csv')

def filter_related(rows):
    '''
    filter out related tweets based on all_drugs list
    '''
    words = rows.split(' ')
    if len(set(words) & set(all_drugs)) != 0:
        return True
    else:
        return False
def identify_drugs(rows):
    words = rows.split(' ')
    return set(words) & set(all_drugs)

# first we filter out drugs related tweets, take 10 as example:
tw_23.take(10)

# they seem to be...everywhere
tw_23.map(identify_drugs).take(10)

#load in labeled dataset:
train = pd.read_excel('Dataset 2 marked.xlsx')

da_train = train[train['Drug Abuse'] == 1]
dna_train = train[train['Drug Non-Abuse'] == 1]

def clean(doc):
    from stop_words import get_stop_words
    from nltk.stem.wordnet import WordNetLemmatizer
    import string
    
    stopw = get_stop_words('en')
    exclude = set(string.punctuation) 
    lemma = WordNetLemmatizer()

    doc = doc.replace('/', ' ')
    stop_free = " ".join([i for i in doc.lower().split() if i not in stopw])
    punc_free = ''.join(ch for ch in stop_free if ch not in exclude)
    normalized = " ".join(lemma.lemmatize(word) for word in punc_free.split())
    return normalized


train['Drug Non-Abuse'] = train['Drug Non-Abuse'].replace(1, 0)
train['label'] = train['Drug Non-Abuse'].combine_first(train['Drug Abuse']).fillna(2)


data2 = train[['Tweets', 'label']].groupby('label').agg(sum)

# compile documents
doc_complete = data2.Tweets
doc_clean = [clean(doc).split() for doc in doc_complete]  
# wdict is the id2token dictionary
wdict = corpora.Dictionary(doc_clean)
# dtm: doc_term_matrix
dtm = [wdict.doc2bow(doc) for doc in doc_clean]

mt =  get_cs_matrix(doc_complete)
np.fill_diagonal(mt, 0)
mtorder = mt.argsort(axis= 1)
topic_group = pd.DataFrame([train.label, 
              train.label[mtorder[-1]].values,
              train.label[mtorder[-2]].values,
             train.label[mtorder[-3]].values]).T
topic_group.columns = ['type', 'most similar', 'second', 'third']

#grouping based on cosine_simlarity of topics
#grouping
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import TfidfVectorizer
def get_cs_matrix(doc):
    tfidf_vectorizer = TfidfVectorizer(stop_words = 'english')
    tfidf_matrix_train = tfidf_vectorizer.fit_transform(doc) 
    cosine_sim = cosine_similarity(tfidf_matrix_train, tfidf_matrix_train)
    return cosine_sim

def grouping(doc):
    matrix = get_cs_matrix(doc)
    
def predict_topic(doc,new_doc):
    doc.set_value(3,new_doc)
    tfidf_vectorizer = TfidfVectorizer(stop_words = 'english')
    tfidf_matrix_train = tfidf_vectorizer.fit_transform(doc)
    csm = cosine_similarity(tfidf_matrix_train[-1], tfidf_matrix_train)

    return csm[0][:-2].argmax()
    #return tfidf_matrix_train


for i in range(1,4):
    print 'true label:', train.loc[i,'label']
    print 'predicted label:', predict_topic(doc_complete, train.Tweets[i], train.label)

pred_labels = []
for new in train.Tweets:
    pred_labels.append(predict_topic(doc_complete, new))

def acc_per(pred, true):
    pred_t = pred
    true_t = true
    return 1.0 * (pred_t == true_t).sum()/len(true)

print acc_per(pred_labels, train.label)

train['pred_labels'] = pred_labels

def perf_measure(y_actual, y_hat):
    TP = 0
    FP = 0
    TN = 0
    FN = 0

    for i in range(len(y_hat)): 
        if y_actual[i]==y_hat[i]==1:
           TP += 1
    for i in range(len(y_hat)): 
        if (y_hat[i]==1) and (y_actual[i]!=y_hat[i]):
           FP += 1
    for i in range(len(y_hat)): 
        if y_actual[i]==y_hat[i]==0:
           TN += 1
    for i in range(len(y_hat)): 
        if (y_hat[i]==0) and (y_actual[i]!=y_hat[i]):
           FN += 1

    return(TP, FP, TN, FN)

print perf_measure(train.label, pred_labels)

new_tw = tw_23.take(100)

for tw in new_tw:
    if predict_topic(doc_complete, tw) == 1:
        print tw